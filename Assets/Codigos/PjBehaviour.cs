using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PjBehaviour : MonoBehaviour
{

    [SerializeField]
    private float m_velocidad = 5f;
    [SerializeField]
    private float m_velocidadAgua = 100f;
    [SerializeField]
    private float m_FuerzaSalto = 5f;
    [SerializeField]
    private bool m_nadar;
    [SerializeField]
    private bool isGrounded;
    [SerializeField]
    private bool MovimientoActivo;

    [SerializeField]
    private GameEvent SelfHit;

    [SerializeField]
    private ScriptableFloat vida;
    [SerializeField]
    private N_Disparos municion;

    [SerializeField]
    private GameEvent PillarMuni;
    
    [SerializeField]
    private GameEvent HPFill;

    [SerializeField]
    private GameEvent Muerte;

    [SerializeField]
    private GameEvent Final;

    private float tiempoMin = 0.5f;
    private float tiempoMax = 1.0f;

    bool toSoon = false;

    private CombatState state = CombatState.Nothing;

    private enum State {MOVEMENT, IDLE, NADAR};
    private enum CombatState { C, V, Nothing, CC, CV };

    private State m_state;

    private bool EstaUsandoEscudo;

    [SerializeField]
    private GameObject Sangre;

    [SerializeField]
    private GameObject PosiReal;

    void Start()
    {

        m_state = State.IDLE;
        isGrounded = true;
        m_velocidad = 5f;
        m_velocidadAgua = 6f;
        m_FuerzaSalto = 5f;
        m_nadar = false;
        MovimientoActivo = true;
        vida.value = 20f;
        HPFill.Raise();
        GetComponent<Gancho>().enabled = false;

    }

    void Update()
    {

        if (vida.value <= 0)
        {
            Muerte.Raise();
        }

        if (!EstaUsandoEscudo)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                StartCoroutine(comboNewInput("C"));
            }
            else if (Input.GetKeyDown(KeyCode.V))
            {
                StartCoroutine(comboNewInput("V"));
            }
        }

        if (MovimientoActivo)
        {

            if (Input.GetKey(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = false;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
            }

        }

        MaquinaEjecutar();

    }

    private void MaquinaIniciar(State NewState)
    {

        switch(NewState)
        {

            case State.IDLE:
                m_state = NewState;
                GetComponent<Animator>().Play("IdleAnimation");
                break;
            case State.MOVEMENT:
                m_state = NewState;
                MovimientoActivo = true;
                GetComponent<Animator>().Play("WalkAnimation");
                break;
            case State.NADAR:
                m_state = NewState;
                if (m_nadar)
                    GetComponent<Animator>().Play("SwimAnimation");
                break;
        }

    }

    private void MaquinaAcabar()
    {

        switch (m_state)
        {

            case State.IDLE:
                //print("acabar idle");
                break;
            case State.MOVEMENT:
                //print("acabar movimiento"); 
                break;
            case State.NADAR:
                //print("acabar nadar");
                break;
        }

    }

    private void MaquinaEjecutar()
    {

        switch (m_state)
        {

            case State.IDLE:
                IdleState();
                break;
            case State.MOVEMENT:
                JumpState();
                break;
            case State.NADAR:
                SwimState();
                break;
        }

    }

    private void ChangeState(State NextState)
    {

        MaquinaAcabar();

        MaquinaIniciar(NextState);

        MaquinaEjecutar();

    }

    private void IdleState()
    {

        if (m_nadar)
        {
            ChangeState(State.NADAR);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || (isGrounded && Input.GetKeyDown(KeyCode.W)))
        {
            ChangeState(State.MOVEMENT);
        }

    }

    private void JumpState()
    {
        if (isGrounded && Input.GetKeyDown(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, m_FuerzaSalto), ForceMode2D.Impulse);
            isGrounded = false;
        }

        if (!m_nadar && !isGrounded && GetComponent<Rigidbody2D>().velocity.y > 0)
        {
            GetComponent<Animator>().Play("JumpAnimation");
        }
        else if (!m_nadar && !isGrounded && GetComponent<Rigidbody2D>().velocity.y < 0)
        {

            GetComponent<Animator>().Play("FallAnimation");

        }
        else if (!m_nadar && GetComponent<Rigidbody2D>().velocity.x != 0)
        {
            GetComponent<Animator>().Play("WalkAnimation");
        }


        if (m_nadar)
        {
            ChangeState(State.NADAR);
        }
        else if (GetComponent<Rigidbody2D>().velocity.x == 0 && GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            ChangeState(State.IDLE);
        }

    }

    private void SwimState()
    {

        if (m_nadar)
        {

            if (Input.GetKeyDown(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidadAgua, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = false;
            }

            else if (Input.GetKeyDown(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidadAgua, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = true;
            }

            if (Input.GetKeyDown(KeyCode.W))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, m_velocidadAgua);
            else if (Input.GetKeyDown(KeyCode.S))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -m_velocidadAgua);
        } else
        {
            m_state = State.IDLE;
        }


    }

    IEnumerator comboNewInput(string input)
    {

        if (!toSoon)
        {
            switch (state)
            {
                case CombatState.Nothing:
                    if (input == "C")
                    {
                        GetComponent<Animator>().Play("Hit_C");
                        state = CombatState.C;
                        print("Hace c");
                    }
                    else if (input == "V")
                    {

                        //otro ataque
                        print("hace v");
                        state = CombatState.Nothing;
                    }
                    break;
                case CombatState.C:
                    if (input == "C")
                    {
                        GetComponent<Animator>().Play("Hit_C_C");
                        print("Hace c_c");
                        state = CombatState.CC;

                    }
                    else if (input == "V")
                    {
                        print("hace c_v");
                        state = CombatState.CV;
                    }
                    break;

                case CombatState.CC:
                    if (input == "C")
                    {
                        // tercer ataque
                        GetComponent<Animator>().Play("Hit_C_C_C");
                        print("Hace c_c_c");
                        state = CombatState.Nothing;// si este fuera el ataque final me pndria como estado inicial, si no seguiria

                    }
                    else if (input == "V")
                    {
                        print("hace c_c_v");
                        state = CombatState.Nothing;
                    }
                    break;
                case CombatState.CV:
                    if (input == "C")
                    {
                        // tercer/2 ataque
                        print("Hace c_v_c");
                        state = CombatState.Nothing;

                    }
                    else if (input == "V")
                    {
                        print("hace v");
                        state = CombatState.Nothing;
                    }
                    break;
            }
        }




        CombatState copyState = state;
        toSoon = true;
        yield return new WaitForSeconds(tiempoMin);
        toSoon = false;
        yield return new WaitForSeconds(tiempoMax - tiempoMin);


        if (state == copyState) //si no a variado lo reiniciamos
        {
            isGrounded = true;
            GetComponent<Animator>().Play("IdleAnimation");
            state = CombatState.Nothing;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "agua")
        {
            m_nadar = false;
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "agua")
        {
            m_nadar = true;
        }

        if (collision.tag == "Vida")
        {
            if (vida.value < 20)
            {
                vida.value++;
            }
            HPFill.Raise();
            GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Municion")
        {
            if (municion.numeroDisparos < municion.MaximoDisparos)
            {
                municion.numeroDisparos++;
                PillarMuni.Raise();
            }
            GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Full")
        {

            vida.value = 20;
            municion.numeroDisparos = municion.MaximoDisparos;
            PillarMuni.Raise();
            HPFill.Raise();
            GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject);

        }

        if (collision.tag == "AreaEfecto")
        {

            for(int i = 0; i < 10; i++)
            {
                SelfHit.Raise();
            }

        }

        if (collision.tag == "Hook")
        {

            GetComponent<Gancho>().enabled = true;
            GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject);

        }

        if(collision.tag == "Final")
        {
            Final.Raise();
        }


    }

    private void OnCollisionEnter2D(Collision2D collision) 
    {

        if (collision.gameObject.tag == "suelo")
        {
            isGrounded = true;
        }

        if (collision.gameObject.tag == "Pinchos")
        {
            isGrounded = true;
            SelfHit.Raise();
            SelfHit.Raise();
        }

        if (collision.gameObject.layer == 9 || collision.gameObject.layer == 10)
        {

            // a�adir repeler como cuando te hitean en el metrodi, no funciona con el AddForce
            if (collision.gameObject.transform.position.x > transform.position.x)
            {
                print("Estoy a la Izquierda");
                StartCoroutine(Impulso(true));
            }
            else
            {
                print("Estoy a la derecha");
                StartCoroutine(Impulso(false));
            }
        }

    }

    private void OnCollisionExit2D(Collision2D collision) 
    {

        if (collision.gameObject.tag == "suelo")
        {
            isGrounded = false;
        }

    }

    public void RecibirDa�o()
    {
        if (EstaUsandoEscudo)
        {
            return;
        }

        vida.value -= 1f;
    }


    IEnumerator Impulso(bool a)
    {
        if (a)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2500, 2), ForceMode2D.Force);
            
        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(2500, 2), ForceMode2D.Force);
        }

        yield return null;
    }

    public void Sangrar()
    {
        if (EstaUsandoEscudo)
        {
            return;
        }
        GameObject a = Instantiate(Sangre);
        a.transform.position = PosiReal.transform.position;
        Destroy(a, 1f);

    }
    public void setEstaUsandoEscudo(bool a) { EstaUsandoEscudo = a;}

}
