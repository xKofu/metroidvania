using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoK_Contoller : MonoBehaviour
{
    //[SerializeField]
    GameObject PlayerTarget;

    bool m_seguir = false;

    float TiempoDeVida;

    [SerializeField]
    GameObject Explosion;

    [SerializeField]
    GameObject Fuego;



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_seguir)
            seguirTarget();
        else
            comportamiento();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            m_seguir = true;
            PlayerTarget = collision.gameObject;
        }
        
    }


    private bool entrarHacerCosas;
    private void seguirTarget()
    {

        gameObject.GetComponent<Animator>().Play("EnemiK_Walk_Animation");
        if (transform.position.x > PlayerTarget.transform.position.x - 0.3 && transform.position.x < PlayerTarget.transform.position.x + 0.3)
        {

        }
        else if (transform.position.x < PlayerTarget.transform.position.x)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(5f, GetComponent<Rigidbody2D>().velocity.y);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (transform.position.x > PlayerTarget.transform.position.x)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-5f, GetComponent<Rigidbody2D>().velocity.y);
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }


        TiempoDeVida += Time.deltaTime;

        if (TiempoDeVida > 5)
        {
            entrarHacerCosas = true;
        }

        if (entrarHacerCosas)
        {


            GameObject a = Instantiate(Explosion);
            a.transform.position = transform.position;

            GameObject b = Instantiate(Fuego);
            b.transform.position = transform.position;


            Destroy(gameObject);
            Destroy(a, 1.0f);

            Destroy(b, 3.0f);


        }
    }





    float tiempo;
    int rutina;
    int direccion;
    [SerializeField]
    float speed = 1f;

    private void comportamiento()
    {
        tiempo += 1 * Time.deltaTime;

        if (tiempo >= 2)
        {
            rutina = Random.Range(0, 2);
            tiempo = 0;
        }

        switch (rutina)
        {
            case 0:
                //print("quieto");
                gameObject.GetComponent<Animator>().Play("EnemigoK_Quieto_Animation"); //che gordito
                break;
            case 1:
                //print("direccion");
                direccion = Random.Range(0, 2);
                rutina++;
                break;
            case 2:
                switch (direccion)
                {
                    case 0:
                        //print("direccion derecha");
                        transform.rotation = Quaternion.Euler(0, 0, 0);

                        transform.Translate(Vector3.right * speed * Time.deltaTime);
                        break;
                    case 1:
                        //print("direccion izquierda");
                        transform.rotation = Quaternion.Euler(0, 180, 0);

                        transform.Translate(Vector3.right * speed * Time.deltaTime); // se sigue manteniendo a la derecha porque lo roto gordito
                        break;
                }
                //animator.SetBool("mover", false);
                gameObject.GetComponent<Animator>().Play("EnemiK_Walk_Animation");
                break;
        }
    }


}

