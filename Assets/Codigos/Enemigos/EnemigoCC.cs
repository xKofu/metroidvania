using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCC : MonoBehaviour
{

    // que si hector que si
    [SerializeField]
    float speed = 10f;

    [SerializeField]
    float rangoVision;


    [SerializeField]
    GameObject rango;

    [SerializeField]
    GameObject Hit;







    [SerializeField]
    private GameObject target;


    Animator animator;
    int rutina;
    int direccion;
    bool atacando;

    float tiempo;







    void Start()
    {
        animator = GetComponent<Animator>();

    }


    void Update()
    {
        comportamiento();
    }


    private void comportamiento()
    {

        if (Mathf.Abs(transform.position.x - target.transform.position.x) > rangoVision && !atacando)
        {


            //print("-- -- Patrulla -- --");
            tiempo += 1 * Time.deltaTime;

            if (tiempo >= 2)
            {
                rutina = Random.Range(0, 2);
                tiempo = 0;
            }

            switch (rutina)
            {
                case 0:
                    //print("quieto");
                    animator.Play("EnemigoCC_Quieto"); //che gordito
                    
                    break;
                case 1:
                    //print("direccion");
                    direccion = Random.Range(0, 2);
                    rutina++;
                    break;
                case 2:
                    switch (direccion)
                    {
                        case 0:
                            //print("direccion derecha");
                            transform.rotation = Quaternion.Euler(0, 0, 0);
                            transform.Translate(Vector3.right * speed * Time.deltaTime);
                            break;
                        case 1:
                            //print("direccion izquierda");
                            transform.rotation = Quaternion.Euler(0, 180, 0);
                            transform.Translate(Vector3.right * speed * Time.deltaTime); // se sigue manteniendo a la derecha porque lo roto gordito
                            break;
                    }
                    //animator.SetBool("mover", false);
                    animator.Play("EnemigoCC_CaminarMover");
                    break;
            }
        }
        else
        {
            //print("-- -- SEGUIR -- --");

            if (Mathf.Abs(transform.position.x - target.transform.position.x) < rangoVision && !atacando && (target.transform.position.y < transform.position.y + 2 && target.transform.position.y > transform.position.y - 2)) // seguir target(player siempre)
            {
                /*
                print("te sigo porque estas dentro de mi rango");
                print("Y arriba " + (transform.position.y+2));
                print("Y mia " + target.transform.position.y);
                print("Y abajo " + (transform.position.y-2));
                */
                if (transform.position.x < target.transform.position.x)
                {
                    //print("Me muevo seguir derecha");
                    animator.Play("EnemigoCC_CaminarMover");
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    transform.Translate(Vector3.right * (speed + 2) * Time.deltaTime); // en plan te esta siguiendo con ganas pues corre un poco mas sbs
                }
                else
                {
                    //print("Me muevo seguir Izquierda");
                    animator.Play("EnemigoCC_CaminarMover");
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                    transform.Translate(Vector3.right * (speed + 2) * Time.deltaTime);
                }
                print("caminar");
            }
            else // rango de ataque
            {
                if (!atacando)
                {
                    if (transform.position.x < target.transform.position.x)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                    }
                    else
                    {
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                    }

                    // tendria que parar o iniciar la animacion de ataque
                }
            }
        }
    }

    public void Parar_Animaciones()
    {
        animator.Play("EnemigoCC_Quieto");
        print("Che gordito E");
        atacando = false;
        rango.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void ColliderAtaqueActivado()
    {
        Hit.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void ColliderAtaqueDesactivado()
    {
        Hit.GetComponent<BoxCollider2D>().enabled = false;
    }


    public void setAtacando(bool a) { atacando = a; }
}
