using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil_enemigo : MonoBehaviour
{

    
    GameObject canon; // ca�on
    GameObject target; // player

    float speed = 10.0f;

    public float time = 2.0f;

    public GameEvent hit;
  
    void Start()
    {
        /*
        float Vx = (target.transform.position.x - canon.transform.position.x)/time;
        float gravity = -9.8f * GetComponent<Rigidbody2D>().gravityScale;
        float Vy = (target.transform.position.y - canon.transform.position.y - (gravity*time*time)/2) / time;    // estas cosas de aqui sin el canon tambin rulan
        GetComponent<Rigidbody2D>().velocity = new Vector2(Vx, Vy);
        */
    }

    
    void Update()
    {
        /*
        float canonX = canon.transform.position.x;
        float targetX = target.transform.position.x;

        float dist = targetX - canonX;
        float nextX = Mathf.MoveTowards(transform.position.x, targetX, speed * Time.deltaTime);
        float baseY = Mathf.Lerp(canon.transform.position.y, target.transform.position.y, (nextX - canonX) / dist);

        float height = 2 * (nextX - canonX) * (nextX - targetX) / (-0.25f * dist * dist);
        Vector3 movePosition = new Vector3(nextX, baseY + height, transform.position.z);


        //transform.rotation = LookAtTarget(movePosition - transform.position);
        transform.position = movePosition;
        */
    }


    public static Quaternion LookAtTarget(Vector2 rotation)
    {
        return Quaternion.Euler(0, 0, Mathf.Atan2(rotation.y, rotation.x) * Mathf.Rad2Deg);
    }

    public void setCanon(GameObject g)
    {
        canon = g;
        float Vx = (target.transform.position.x - canon.transform.position.x) / time;
        float gravity = -9.8f * GetComponent<Rigidbody2D>().gravityScale;
        float Vy = (target.transform.position.y - canon.transform.position.y - (gravity * time * time) / 2) / time;    // estas cosas de aqui sin el canon tambin rulan
        GetComponent<Rigidbody2D>().velocity = new Vector2(Vx, Vy);
    }

    public void setTarget(GameObject g)
    {
        target = g;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            canon.GetComponent<EnemigoDistancia>().devolverALaPool(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*
        if (collision.gameObject.tag == "Escudo" || collision.gameObject.layer == 22)
        {

            return;
        }
        */

        if (collision.gameObject.tag == "Player")
        {
            if (canon != null)
            {
                canon.GetComponent<EnemigoDistancia>().devolverALaPool(gameObject);
                hit.Raise();
            }

        }



        if (collision.gameObject.tag != "Enemigo_Distancia")
        {
            //print("impacto vs " + collision.name);
        }


        if (collision.gameObject.tag == "LimiteMapa")
        {
            if (canon != null)
            {
                canon.GetComponent<EnemigoDistancia>().devolverALaPool(gameObject);
            }
            
        }
  
        
        
    }

}
