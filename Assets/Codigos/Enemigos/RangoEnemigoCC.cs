using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigoCC : MonoBehaviour
{
    [SerializeField]
    Animator m_animator;

    [SerializeField]
    Enemigo_CC m_enemigo;



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            m_animator.Play("EnemigoCC_Atacar");
            m_enemigo.setAtacando(true);
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
