using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigo_CC : MonoBehaviour
{
    [SerializeField]
    EnemigoDistancia m_enemigoD;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            m_enemigoD.setTarget(collision.gameObject);
            m_enemigoD.PoderDisparar(true);

            
        }
    }


    // perdoname hector por esto pero al fin y al cabo es lo que hay
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Proyectil" || collision.gameObject.layer == 6)
        {
            
            m_enemigoD.subirVida(2);
        }
        else if(collision.tag == "HitPlayer"){
            m_enemigoD.subirVida(1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            m_enemigoD.PoderDisparar(false);
            m_enemigoD.GetComponent<Animator>().Play("EnemigoD_Quieto");
        }
    }
}
