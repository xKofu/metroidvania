using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDistancia : MonoBehaviour
{

    private bool poderDisparar;
    [SerializeField]
    Pool p;


    private GameObject target;

    [SerializeField]
    private Transform PuntaCanon;

    [SerializeField]
    private GameObject Particulas;

    [SerializeField]
    private GameEvent m_hit;


    [SerializeField]
    private int m_Vida = 3;


    void Start()
    {
        StartCoroutine(disparar());
        

    }

    void Update()
    {
        if (!poderDisparar)
        {
            comportamiento();
        }


        if (m_Vida <= 0)
        {
            Destroy(this.gameObject);
        }

    }

    private IEnumerator disparar()
    {
        while (true)
        {
            if (poderDisparar)
            {

                gameObject.GetComponent<Animator>().Play("EnemigoD_Disparar");
                GameObject proyectil = p.getElement();
                proyectil.transform.position = gameObject.transform.position;
                proyectil.GetComponent<Proyectil_enemigo>().setTarget(target);
                proyectil.GetComponent<Proyectil_enemigo>().setCanon(gameObject);
                proyectil.GetComponent<Proyectil_enemigo>().hit = m_hit;
                // pongo las particulas // el colab no va
                Instantiate(Particulas, PuntaCanon);


                if (target.transform.position.x > gameObject.transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                }

            }
            yield return new WaitForSeconds(2.0f); ;
        }
        
    }


    public void setTarget(GameObject target_)
    {
        target = target_;
    }

    public void PoderDisparar(bool poderDisparar_)
    {
        poderDisparar = poderDisparar_;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Proyectil")
        {
            m_Vida -= 2;
        }
        if (collision.gameObject.tag == "HitPlayer" || collision.gameObject.tag == "HitEnemigo")
        {
            m_Vida--;
        }

    }





















    public void devolverALaPool(GameObject a)
    {
        p.ReturnElement(a);
    }




    float tiempo;
    int rutina;
    int direccion;
    [SerializeField]
    float speed = 10f;


    private void comportamiento()
    {
        tiempo += 1 * Time.deltaTime;

        if (tiempo >= 2)
        {
            rutina = Random.Range(0, 2);
            tiempo = 0;
        }

        switch (rutina)
        {
            case 0:
                //print("quieto");
                gameObject.GetComponent<Animator>().Play("EnemigoD_Quieto"); //che gordito
                break;
            case 1:
                //print("direccion");
                direccion = Random.Range(0, 2);
                rutina++;
                break;
            case 2:
                switch (direccion)
                {
                    case 0:
                        //print("direccion derecha");
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        
                        transform.Translate(Vector3.right * speed * Time.deltaTime);
                        break;
                    case 1:
                        //print("direccion izquierda");
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        
                        transform.Translate(Vector3.right * speed * Time.deltaTime); // se sigue manteniendo a la derecha porque lo roto gordito
                        break;
                }
                //animator.SetBool("mover", false);
                gameObject.GetComponent<Animator>().Play("EnemigoD_Mover");
                break;
        }
    }



    public void subirVida(int a)
    {
        m_Vida+=a;
    }
  
}
