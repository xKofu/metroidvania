using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{

    [SerializeField]
    private Image HealthBar;

    public float vidaTotal = 20;

    [SerializeField]
    private ScriptableFloat vidaActual;

    void Start()
    {
        HealthBar.type = Image.Type.Filled;
        HealthBar.fillMethod = Image.FillMethod.Horizontal;
        HealthBar.fillAmount = vidaActual.value / vidaTotal;
        //HealthBar.fillOrigin = Image.OriginHorizontal;

    }

    public void Fill()
    {
        HealthBar.fillAmount = vidaActual.value / vidaTotal;
        GetComponent<AudioSource>().Play();
    }

    public void FillG()
    {
        HealthBar.fillAmount = vidaActual.value / vidaTotal;
    }
}
