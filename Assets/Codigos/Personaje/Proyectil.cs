using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{

    private Rigidbody2D _rigidbody;
    
    public delegate void Impacto(GameObject ob);
    public event Impacto ImpactoProyectil;
    
    private Vector3 direccionP;
    
    private void Start()
    {
        //print("existo");
    }

    private void Awake()
    {
        

    }

   
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = transform.right*10;
        //GetComponent<Rigidbody2D>().velocity = new Vector2(5f, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.layer == 9 || collision.gameObject.layer == 7)
            ImpactoProyectil(gameObject);

            
    }



}
