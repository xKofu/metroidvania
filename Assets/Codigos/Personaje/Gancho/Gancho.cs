using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gancho : MonoBehaviour
{
    [SerializeField]
    LineRenderer line;

    [SerializeField]
    Transform Reference;

    [SerializeField] LayerMask grapplableMask;
    [SerializeField] float maxDistance = 10f; // si lo quito puedo tirar el gancho desde donde quiera


    bool isGrappling = false;
    [HideInInspector] public bool retracting = false;

    Vector2 target;

    RaycastHit2D hit;

    private void Start()
    {
        //line = GetComponent<LineRenderer>();
    }



    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && !isGrappling)
        {
            StartGrapple();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            gameObject.GetComponent<DistanceJoint2D>().enabled = false;
            isGrappling = false;
            transform.position = Reference.position;
        }


        if (isGrappling)
        {
            line.SetPosition(0, transform.position);
            line.SetPosition(1, hit.point);
        }

        line.enabled = isGrappling;

    }

    private void StartGrapple()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition)- transform.position;

        //Debug.DrawLine(transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition), Color.blue, 10.0f);
        hit = Physics2D.Raycast(transform.position, direction, maxDistance, grapplableMask); //RaycastHit2D 


        if (hit.collider != null)
        {
            Debug.DrawLine(transform.position, new Vector2(hit.point.x, hit.point.y), Color.blue, 10.0f);
            print(hit.collider.IsTouchingLayers(13));
            print(hit.collider.gameObject.name + " " + hit.collider.gameObject.layer);
        }


        

        if (hit.collider != null && hit.collider.gameObject.layer==13)
        {

            //print(hit.collider.IsTouchingLayers(13));
            //print(hit.transform.position);
            // si el raycaste detecta una zona donde me puedo enganchar hago cosas entre ellas el join

            isGrappling = true;
            line.enabled = true;
            line.positionCount = 2;



            gameObject.GetComponent<Rigidbody2D>().MovePosition(Vector2.MoveTowards(transform.position, hit.transform.position, 2 * Time.deltaTime));

            gameObject.GetComponent<DistanceJoint2D>().enabled = true;
            //gameObject.GetComponent<DistanceJoint2D>().connectedAnchor = new Vector2(hit.transform.position.x, hit.transform.position.y);

            gameObject.GetComponent<DistanceJoint2D>().connectedAnchor = new Vector2(hit.point.x, hit.point.y);


            print(new Vector2(hit.point.x, hit.point.y));


            //float distance = Mathf.Max(Vector2.Distance(gameObject.transform.position, hit.point) / 2, 3.0f);
            

            gameObject.GetComponent<DistanceJoint2D>().distance = 3.0f;
            target = hit.point;

            
        }
    }

   
    //https://www.youtube.com/watch?v=dnNCVcVS6uw
    //https://www.youtube.com/watch?v=P-UscoFwaE4
    

}
