using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Gancho : MonoBehaviour
{

    Rigidbody2D rb;
    [SerializeField]
    float speed = 5.0f;

    float mx;
    float my;


    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        
    }

    
    void Update()
    {
        mx = Input.GetAxis("Horizontal");
        my = Input.GetAxis("Vertical");
    }



    private void FixedUpdate()
    {
        rb.velocity = new Vector2(mx * speed, my).normalized * speed;
    }
}
