using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtaques : MonoBehaviour
{

    [SerializeField]
    Animator animator;

    [SerializeField]
    Transform HitBox_ataque;

    [SerializeField]
    float rango_HitBox_ataque = 0.2f;


    [SerializeField]
    LayerMask enemyLayer;


    float tiempoEntreAtaque = 2f;

    float siguienteAtaque = 0f;




    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= siguienteAtaque)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ataques();
                siguienteAtaque = Time.time + 2f / tiempoEntreAtaque;
            }
        }
    }

    void ataques()
    {
        animator.SetTrigger("Ataque");
        Collider2D[] hitEnemigos = Physics2D.OverlapCircleAll(HitBox_ataque.position, rango_HitBox_ataque, enemyLayer);


        foreach (Collider2D enemy in hitEnemigos)
        {
            Debug.Log("We hit " + enemy.name);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (HitBox_ataque == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(HitBox_ataque.position, rango_HitBox_ataque);
    }



}
