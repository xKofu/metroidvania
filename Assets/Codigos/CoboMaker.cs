using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoboMaker : MonoBehaviour
{
    
    private float tiempoMin = 0.2f;
    private float tiempoMax = 1.0f;


    private CombatState state = CombatState.Nothing; // esto sera un algo que sabe el gerar (maquina de estados y tal)

    private enum CombatState { C, V, Nothing, CC, CV };

    bool toSoon = false;

    void Start()
    {
        
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(comboNewInput("C"));
        }
        else if (Input.GetKeyDown(KeyCode.V))
        {
            StartCoroutine(comboNewInput("V"));
        }
    }


    IEnumerator comboNewInput(string input)
    {

        if (!toSoon) { 
            switch (state)
            {
                case CombatState.Nothing:
                    if (input == "C")
                    {
                        // primer ataque
                        state = CombatState.C;
                        print("Hace c");
                    }
                    else if (input == "V")
                    {

                        //otro ataque
                        print("hace v");
                        state = CombatState.Nothing;
                    }
                    break;
                case CombatState.C:
                    if (input == "C")
                    {
                        // Segundo ataque
                        print("Hace c_c");
                        state = CombatState.CC;

                    }
                    else if (input == "V")
                    {
                        print("hace c_v");
                        state = CombatState.CV;
                    }
                    break;

                case CombatState.CC:
                    if (input == "C")
                    {
                        // tercer ataque
                        print("Hace c_c_c");
                        state = CombatState.Nothing;// si este fuera el ataque final me pndria como estado inicial, si no seguiria

                    }
                    else if (input == "V")
                    {
                        print("hace c_c_v");
                        state = CombatState.Nothing;
                    }
                    break;
                case CombatState.CV:
                    if (input == "C")
                    {
                        // tercer/2 ataque
                        print("Hace c_v_c");
                        state = CombatState.Nothing;

                    }
                    else if (input == "V")
                    {
                        print("hace v");
                        state = CombatState.Nothing;
                    }
                    break;
            }
        }




        CombatState copyState = state;
        toSoon = true;
        yield return new WaitForSeconds(tiempoMin);
        toSoon = false;
        yield return new WaitForSeconds(tiempoMax-tiempoMin);

        
        if (state == copyState) //si no a variado lo reiniciamos
        {
            state = CombatState.Nothing;
        }
    }

}
