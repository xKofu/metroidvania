using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibracionCamara : MonoBehaviour
{

    [SerializeField]
    float duracion = 1.0f; // ara toco


    [SerializeField]
    bool Shaking = false; // en ingles queda mejor

    [SerializeField]
    private AnimationCurve curve;


    // Update is called once per frame
    void Update()
    {
        if (Shaking)
        {
            Shaking = false;
            StartCoroutine(Vibras());
        }
    }

    IEnumerator Vibras()
    {
        Vector3 posicionInicial = transform.position;
        float timepo = 0f;
        while (timepo < duracion)
        {
            timepo += Time.deltaTime;
            float fuerzaVibracion = curve.Evaluate(timepo / duracion);
            transform.position = posicionInicial + Random.insideUnitSphere * fuerzaVibracion;
            yield return null;
        }
        transform.position = posicionInicial;
    }


    public void setShaking(bool shake) { Shaking = shake;}

}
