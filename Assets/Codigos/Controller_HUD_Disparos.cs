using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.UIElements;


public class Controller_HUD_Disparos : MonoBehaviour
{

    [SerializeField]
    private TMPro.TextMeshProUGUI textoMunicion;

    [SerializeField]
    private N_Disparos m_dis;

    

    void Start()
    {
        int a = m_dis.numeroDisparos;
        textoMunicion.text = "" + a;
    }


    void Update()
    {
        //mirarN_Disparos();
    }


    public void mirarN_Disparos()
    {

        textoMunicion.text = "" + m_dis.numeroDisparos;

    }
}
