using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    [SerializeField]
    private Sprite openDoor;
    [SerializeField]
    private Sprite closedDoor;

    [SerializeField]
    private GameObject texto;


    private bool HaTocadoPlayer;

    private float contador;

    private void Start()
    {
        GetComponent<SpriteRenderer>().sprite = closedDoor;
        GetComponent<BoxCollider2D>().isTrigger = false;
    }

    public void Open() 
    {
        print("Se abre puerta");
        this.GetComponent<SpriteRenderer>().sprite = openDoor;
        this.GetComponent<BoxCollider2D>().enabled = false;
        print(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            HaTocadoPlayer = true;
  
        }    
    }


    private void Update()
    {
        if (HaTocadoPlayer)
        {
            texto.SetActive(true);
            contador += Time.deltaTime;

            if (contador > 4)
            {
                HaTocadoPlayer = false;
                texto.SetActive(false);
                contador = 0;

            }
            
        }

    }

}
