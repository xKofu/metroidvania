using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPJ : MonoBehaviour
{
    [SerializeField]
    private float m_velocidad;
    [SerializeField]
    private float m_velocidadAgua;
    [SerializeField]
    private float m_FuerzaSalto;
    [SerializeField]
    private bool m_suelo;
    [SerializeField]
    private bool m_nadar;

    private enum State { AIRE, CORRER, IDLE, NADAR };

    private State m_state;

    void Start()
    {
        m_state = State.IDLE;
        m_suelo = true;
        m_velocidad = 5f;
        m_velocidadAgua = 3f;
        m_FuerzaSalto = 300f;
        m_nadar = false;
    }

    // Update is called once per frame
    void Update()
    {

        
        if (!m_nadar)
        {
            if (m_suelo && Input.GetKey(KeyCode.W))
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, m_FuerzaSalto));
                //GetComponent<Animator>().Play("JumpAnimation");
                m_suelo = false;
            }

            if (Input.GetKey(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = false;
                //GetComponent<Animator>().Play("WalkAnimation");
            }     
            else if (Input.GetKey(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = true;
                //GetComponent<Animator>().Play("WalkAnimation");
            }   
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
                //GetComponent<Animator>().Play("IdleAnimation");
            }
                
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidadAgua, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = false;
            }

            else if (Input.GetKeyDown(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidadAgua, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<SpriteRenderer>().flipX = true;
            }

            if (Input.GetKeyDown(KeyCode.W))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, m_velocidadAgua);
            else if (Input.GetKeyDown(KeyCode.S))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -m_velocidadAgua);
        }

        if (GetComponent<Rigidbody2D>().velocity.y > 0)
        {
            GetComponent<Animator>().Play("JumpAnimation");
        } else if (GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            //m_suelo = false;
            GetComponent<Animator>().Play("FallAnimation");
        }

        if (m_suelo && GetComponent<Rigidbody2D>().velocity.x != 0)
        {
            GetComponent<Animator>().Play("WalkAnimation");
        } else if (m_suelo && GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            GetComponent<Animator>().Play("IdleAnimation");
        }


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!m_nadar && !m_suelo && collision.collider.tag == "suelo")
        {

            m_suelo = true;
            //GetComponent<Animator>().Play("IdleAnimation");

        }
        
                 
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (!m_nadar && m_suelo && collision.collider.tag == "suelo")
        {

            m_suelo = false;
            //GetComponent<Animator>().Play("IdleAnimation");

        }


    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "agua")
        {
            m_nadar = false;
            m_suelo = false;
        }
            
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "agua")
        {
            m_nadar = true;
            //m_suelo = false;
        }
            
        
    }
}
