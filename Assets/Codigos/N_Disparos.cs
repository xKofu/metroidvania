using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class N_Disparos : ScriptableObject
{
    public int numeroDisparos;
    public int MaximoDisparos;
}
