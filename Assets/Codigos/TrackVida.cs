using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackVida : MonoBehaviour
{
    [SerializeField]
    ScriptableFloat vida;

    public void ActualizarVida()
    {
        GetComponent<TMPro.TextMeshProUGUI>().text = vida.value + "";
    }
}
