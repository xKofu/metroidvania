using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EsceneManager : MonoBehaviour
{

    public void OnDeath()
    {
        SceneManager.LoadScene("Muerte");
    }

    public void OnFinal()
    {
        SceneManager.LoadScene("Final");
    }

}
