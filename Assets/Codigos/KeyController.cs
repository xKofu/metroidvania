using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{

    [SerializeField]
    private GameEvent m_event;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            m_event.Raise();
            print("Abrir Puerta");
            Destroy(gameObject);
        }


    }

}
