using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara_Controller : MonoBehaviour
{
    [SerializeField]
    private GameObject TargetPlayer;

    private Vector3 m_Offset;
    private Vector3 m_FollowPosition;
    private Vector3 m_InitialPosition;

    private float m_FollowSpeed = 0.3f;
    private float m_FollowTime = 0.0f;



    void Start()
    {
        if (TargetPlayer)
        {
            m_FollowPosition = TargetPlayer.transform.position;
            m_InitialPosition = transform.position;
            m_Offset = TargetPlayer.transform.position - transform.position;
            m_FollowTime = m_FollowSpeed;
        }

    }

    //lerp
    void Update()
    {
        if (TargetPlayer)
        {
            if (TargetPlayer.transform.position != m_FollowPosition)
            {
                m_InitialPosition = transform.position;
                m_FollowPosition = TargetPlayer.transform.position;
                m_FollowTime = 0.0f;
            }

            if (m_FollowTime < m_FollowSpeed)
            {
                m_FollowTime += Time.deltaTime;
                transform.position = Vector3.Lerp(m_InitialPosition, m_FollowPosition - m_Offset, (m_FollowTime / m_FollowSpeed));
                Debug.DrawLine(transform.position, m_FollowPosition - m_Offset, Color.red);
            }
        }


    }


}
