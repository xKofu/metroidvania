using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paimon_Controller : MonoBehaviour
{
    [SerializeField]
    private GameObject TargetPlayer;

    private Vector3 m_Offset;
    private Vector3 m_FollowPosition;
    private Vector3 m_InitialPosition;



    private float m_FollowSpeed = 0.5f;
    private float m_FollowTime = 0.0f;

    [SerializeField]
    private Pool Pool_Proyectiles;

    private Vector3 mousePosition;

    [SerializeField]
    private N_Disparos m_numeroDisparos;


    [SerializeField]
    private GameEvent HeDisparado;



   

    void Start()
    {
        m_numeroDisparos.numeroDisparos = m_numeroDisparos.MaximoDisparos;
        transform.position = new Vector3(TargetPlayer.transform.position.x, TargetPlayer.transform.position.y + 2f);
        if (TargetPlayer)
        {
            m_FollowPosition = TargetPlayer.transform.position;
            m_InitialPosition = transform.position;
            m_Offset = TargetPlayer.transform.position - transform.position;
            m_FollowTime = m_FollowSpeed;
        }

        

    }


    bool EscudoActivo;
    //lerp
    void Update()
    {
        if (TargetPlayer && !EscudoActivo)
        {
            if (TargetPlayer.transform.position != m_FollowPosition)
            {
                m_InitialPosition = transform.position;
                m_FollowPosition = TargetPlayer.transform.position;
                m_FollowTime = 0.0f;
            }

            if (m_FollowTime < m_FollowSpeed)
            {
                m_FollowTime += Time.deltaTime;
                transform.position = Vector3.Lerp(m_InitialPosition, m_FollowPosition - m_Offset, (m_FollowTime / m_FollowSpeed));
                Debug.DrawLine(transform.position, m_FollowPosition - m_Offset, Color.red);
            }
        }




        //target proyectiles
        if (Input.GetMouseButtonDown(0) && !EscudoActivo && m_numeroDisparos.numeroDisparos > 0)
        {

            mousePosition = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized;
            mousePosition.z = 0;

            float angle = Mathf.Atan2(mousePosition.y, mousePosition.x) * Mathf.Rad2Deg;

            
            GameObject p = Pool_Proyectiles.getElement();
            p.GetComponent<Proyectil>().ImpactoProyectil += DevolverPool;
            p.transform.position = gameObject.transform.position;


            p.transform.localEulerAngles = new Vector3(0, 0, angle);


            BajarDisp();
            HeDisparado.Raise();
            // bajo el numero de disparos
            

    
        }


        if (Input.GetKeyDown(KeyCode.E))
        {
            EscudoActivo = true; 
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            EscudoActivo = false;
        }


        if (EscudoActivo)
        {
            transform.position = TargetPlayer.transform.position;
            gameObject.tag = "Escudo";
            this.GetComponent<CircleCollider2D>().enabled = EscudoActivo;
            this.GetComponent<Animator>().Play("EscudoOrbe");
            TargetPlayer.GetComponent<PjBehaviour>().setEstaUsandoEscudo(EscudoActivo);
            
        }
        else
        {
            gameObject.tag = "Paimon";
            this.GetComponent<CircleCollider2D>().enabled = EscudoActivo;
            this.GetComponent<Animator>().Play("OrbAnimation");
            TargetPlayer.GetComponent<PjBehaviour>().setEstaUsandoEscudo(EscudoActivo);
        }
   

    }

    public void DevolverPool(GameObject a)
    {
        a.GetComponent<Proyectil>().ImpactoProyectil -= DevolverPool;
        Pool_Proyectiles.ReturnElement(a);
    }



    //funcion a la que llamo del evento y tal
    public void BajarDisp()
    {
        m_numeroDisparos.numeroDisparos--;
        
    }
}
